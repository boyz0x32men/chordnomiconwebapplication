# Chordnomicon Web Application

The Chord-nom-icon is a collection of all possible musical chords and their names. This web application also allows users to build chord progressions and displays pictorial aids for the harmonic relationships between chords.

### Installation and Running:

  > Requires Visual Studio Community 2017

Clone repo from bitbucket:

```sh
$ git clone https://yourNameHere@bitbucket.org/boyz0x32men/chordnomiconwebapplication.git
```

To build and run project:

1. Open solution in VS
2. Goto tools > Nuget Package Manager > Manage Nuget Packages for Solution
     * At top, there should be a warning: "Some Nuget packages are missing from this solution. Click to restore from your inline package sources."
3. Click "Restore"
4. Accept any licenses and/or warnings about overwriting existing files
    * After restore completes, there should still be two packages in updates list, you may ignore them.
5. Should be ready to build and run.
